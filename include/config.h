#pragma once

/***************************************************************************************************
                    General Stuff
****************************************************************************************************/
//Versions
#define SW_VERSION "0.0.1" //Software revision
#define HW_VERSION "0.0.1" //Hardware Revision
#define CARD_VERSION "0.0.1" //Version of the card layout
#define CARD_API_VERSION "" //Version of the Arduino api
#define DISPLAY_API_VERSION "" //Version of the tft api

#define HW_VARIANT "A" //Which hardware variant the current terminal is running

#define STAGE_DEBUG 0
#define STAGE_RELEASE 1
#define SOFTWARE_STAGE STAGE_RELEASE

//Logging
#define DEFAULT_LOG_LEVEL logDEBUG //What the current log level is

//Price Config
#define PAUSCHAL_PREIS 3
#define MASTER_PRICE_INCREMENT 100


/***************************************************************************************************
                    Display Options
****************************************************************************************************/
#define USE_SSD_1306 0
#define USE_ST7735_LCD 1
#define USE_PV02003AR 2
#define USING_DISPLAY USE_ST7735_LCD


#define OLED_WIDTH 128 // OLED display width, in pixels
#define OLED_HEIGHT 32 // OLED display height, in pixels

#define LCD_WIDTH 160 // LCD display width, in pixels
#define LCD_HEIGHT 80 // LCD display height, in pixels

//Console out options
//How many line should be displayed in terminal mode
#define LCD_CONSOLE_LINES 9
#define OLED_CONSOLE_LINES 4

#define OLED_FONT_SIZE 1
#define TFT_FONT_SIZE 1

#define MENU_HOLD_TIME 300


/***************************************************************************************************
                    I/O
****************************************************************************************************/
#define BTN_PREV 17
#define BTN_NEXT 5
#define BTN_BACK 16
#define BTN_ENTER 4

#define LED_STATUS 15
#define BUZZER 2

#define SD_CS 32

#define NFC_RST_PIN 33
#define NFC_IRQ_PIN 35
#define NFC_SS_PIN 5


/***************************************************************************************************
                    PWM
****************************************************************************************************/
#define PWM_FREQ 5000
#define BZZ_DIVIDER 5
#define BZZ_ON_TIME 50


/***************************************************************************************************
                    NFC Options
****************************************************************************************************/
#define USE_PN532 0
#define USE_MFRC522 1
#define CARD_MODULE USE_PN532

#define READ_DELAY 200

#define MASTER_CARD_UID "427019589"


/***************************************************************************************************
                    Cryptography Options
****************************************************************************************************/
//Hash Algos
typedef enum{
    NO_HASH = 0,
    BLAKE2S,
    BLAKE2B,
    BLAKE3,
    SHA3_512,
    SHA3_256
}EHashAlgos;

//Sym Crypto Algo
#define AES_265 0
#define AES_128 1

#define HASH_ALGO NO_HASH
#define CRYPT_ALGO AES_256

#define HASH_SIZE 64



/***************************************************************************************************
                    FileSystem Options
****************************************************************************************************/
#define USE_SPIFFS 0
#define USE_SD_CARD 1
#define USE_SD_ANF_SPIFFS 2
#define USE_EEPROM 3

#define FS_LOCATION USE_SPIFFS

#define FORMAT_SPIFFS false

#define USE_SQLITE3 0 //Currently not really planned as its too inconvenient compared to csv
#define USE_CSV 1

#if USE_CSV
    #define TRANSACTIONS_FILE "/transactions.csv"
    #define USERS_FILE "/users.csv"
#endif

#define MAX_COLUMNS_IN_CSV 4

/***************************************************************************************************
                    Return Codes
****************************************************************************************************/
typedef enum{
    eOK = 0,
    eGENERAL_WARN,
    eGENERAL_FAULT,
    eREAD_ISSUE,
    ePERMISSION_DENIED,
    eFILE_IO_ERROR
}EReturnCodes;