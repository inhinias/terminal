#ifndef logging_h
#define logging_h

#include <Arduino.h>

typedef enum{
    logDEBUG,
    logINFO,
    logWARN,
    logERROR
}eLogLevel;

typedef struct{
    uint8_t code;
    String returnMessage;
}rcode;


void log(String);
void log(rcode);
void log(String, eLogLevel);
void setlogLevel(eLogLevel);
eLogLevel getLogLevel();
//log* getLog(); //Method to return the current log

#endif