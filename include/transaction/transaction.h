#ifndef transaction_h
#define transaction_h

#include <Arduino.h>
#include <SPI.h>
#include "card.h"
#include "db/db.h"
#include "config.h"

#if CARD_MODULE == USE_MFRC522
    #include <MFRC522.h>
#elif CARD_MODULE == USE_PN532
    #include <Adafruit_PN532.h>
#endif

class Transaction{
    public:
        Transaction();
        ~Transaction();

        rcode init();

        //Methods used to transact and handle cards
        rcode makeTransaction(tTransaction&); //Write this data to the card and rotate the key
        rcode readCard(tCardMem&); //Get all the data from the card
        bool newCardPresent(String&);


        //methods used to set up a card
        rcode formatCard(); //Cpmpletely wipe the card
        rcode updateCardTrailer(tTrailer); //Update the trailer which defines the sector key


        //Methods to handle transaction lists
        rcode exportToSD(tTransaction); //Export a transcation to the SD card
        rcode exportToEEPROM(tTransaction); //Export a transaction to the EEPROM
        rcode exportToSPIFFS(tTransaction); //Export a transaction to the EEPROM

        rcode readFromSD(uint16_t, tTransaction&); //Return the transaction at index on the SD
        rcode readFromEEPROM(uint16_t, tTransaction&); //Return the transaction at index on the EEPROM
        rcode readFromSPIFFS(uint16_t, tTransaction&); //Return the transaction at index on the EEPROM

        rcode restoreTransactionsFromSD(); //Restore all transactions from the SD-Card
    
    private:
        rcode writeBlock(uint8_t, String); //Write a block on the card
        rcode deleteBlock(uint8_t); //Clear a block on the card
};

#endif