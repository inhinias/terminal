#pragma once
#include <Arduino.h>

typedef struct{
    String cardVersion;
    String hwVersion;
    String swVersion;
    String key; //Hashed Key
}tCardMem;

typedef struct{
    String secretA;
    String secretB;
    byte accessBytes[3];
}tTrailer;

typedef struct{
    String transactorCardID;
    String transactionID;
    String transactorName;
    uint16_t ammount;
    bool isPayment; //true when transaction is a payment (Seen from card), false when a credit
    String key; //Hashed Key Value
}tTransaction;

typedef struct{
    String transactorID;
    String username;
    String cardId;
}tUser;