#pragma once

typedef enum{
    eNoMenu = 0,
    eMenuDrinkType,
    eMenuDrinks,
    eMenuKeypad,
    eMenuMaster,
    eMenuTransact,
    eMenuDeposit,
    eMenuCredit,
    eMenuNotEnoughCredit,
    eMenuPauschal,
    eMenuAskCard,
    eMenuPaid,
    eNUMBER_MENUS
}EMenu;

typedef enum{
    eNoDrink = 0,
    eBier,
    eLongDrink,
    eShot,
    eWein,
    eKaffe,
    eAlcFree,
    eNUMBER_DRINK_TYPES
}EDrinksType;

typedef struct{
    EDrinksType menu; // The Menu the drink should appear in
    String name;
    uint16_t price; // Price in cents
}SDrink;