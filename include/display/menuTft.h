#pragma once

#include <Arduino.h>
#include <TFT_eSPI.h>
#include "helper/logging.h"
#include "config.h"
#include "display/menuList.h"

#define BLACK 0x0000
#define WHITE 0xFFFF
#define GREY  0x5AEB

#define ARROW_WIDTH 15
#define ARROW_HEIGHT 20
#define FONT_HEIGHT 20
#define BOTTOM_OFFSET 8
#define MAX_TEXT_WIDTH 123

class MenuTFT{
    public:
        MenuTFT();
        ~MenuTFT();

        rcode init();
        rcode open(EMenu);
        rcode open(EMenu, uint16_t);
        EMenu getCurrentMenu();
        EDrinksType getCurrentDrinkType();
        SDrink getCurrentDrink();
        uint16_t getCurrentItemIndex();
        String menuToText(EMenu menu);

        void writeToConsole(const String& text);
    
    private:
        void resetDisplay();
        TFT_eSPI dsp;
        EMenu currentMenu;
        EDrinksType currentDrinkType;
        SDrink currentDrink;
        uint16_t currentItemIndex;
        
};