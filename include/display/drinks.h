#include <Arduino.h>
#include "display/menuList.h"

class Drinks{
    public:
        Drinks();
        ~Drinks();

        std::vector<SDrink> getAllDrinks();
        std::vector<SDrink> getDrinksForType(EDrinksType);
        uint8_t getDrinkIndexInItsTypeList(SDrink drink);
    
    private:
        std::vector<SDrink> drinkList;
};

String drinkTypeToText(EDrinksType type);