#pragma once

#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include "helper/logging.h"
#include "config.h"
#include "display/menuList.h"

class MenuOLED{
    public:
        MenuOLED();
        ~MenuOLED();

        rcode init();
        rcode open(EMenu);
        rcode open(EMenu, uint16_t);
        EMenu getCurrentMenu();
        EDrinksType getCurrentDrinkType();
        SDrink getCurrentDrink();
        uint16_t getCurrentItemIndex();
        String menuToText(EMenu menu);

        void writeToConsole(const String& text);
    
    private:
        void resetDisplay();
        Adafruit_SSD1306 dsp;
        EMenu currentMenu;
        EDrinksType currentDrinkType;
        SDrink currentDrink;
        uint16_t currentItemIndex;
        
};