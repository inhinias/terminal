#ifndef fs_h
#define fs_h

#include <FS.h>
#include "config.h"
#if FS_LOCATION == USE_SPIFFS
    #include <SPIFFS.h>
#elif FS_LOCATION == USE_SD_CARD
    #include <SD.h>
    #include <SD_MMC.h>
    #include <SPI.h>
#elif FS_LOCATION == USE_EEPROM
    #include <EEPROM.h>
#else
    #include <SPIFFS.h>
    #include <SD_MMC.h>
    #include <SD.h>
#endif

#endif