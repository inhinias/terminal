#pragma once

#include <Arduino.h>
#include "helper/logging.h"
#include "transaction/card.h"

class DB{
    public:
        DB();
        ~DB();

        rcode init();

        //Data aquisition
        rcode getTransactionByID(String, tTransaction&); //Return a specific transaction for a given uid
        rcode getUserIDByCardId(String, tUser&); //Return user data for a given transactorCardId
        rcode getUserTransactions(String, std::vector<tTransaction>&); //Return vector of all found transactions for a given transactorCardId
        rcode getUserNetWorth(String, uint16_t&); //Return a users net worth for a given transactorCardId

        //requires to have all transactions/users in ram which may result in too high usage of it for long ass tables
        rcode getAllTransactions(std::vector<tTransaction>&);
        rcode getAllUsers(std::vector<tUser>&);

        rcode insertTransaction(tTransaction&);
        rcode updateTransaction(tTransaction&);
        rcode deleteTransaction(tTransaction&);
    
    private:
        rcode createTables();
        rcode getField(String, uint16_t, String&);
        rcode getFields(String, std::vector<String>&);
        rcode fieldToTransaction(String, tTransaction&);
        rcode fieldToUser(String, tUser&);
};