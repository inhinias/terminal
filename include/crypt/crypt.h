#ifndef crypt_h
#define crypt_h

#include <Arduino.h>
#include "helper/logging.h"

class Crypt{
    public:
        Crypt();
        ~Crypt();

        rcode hash(String, String&);
        rcode encrypt(String, String&);
        rcode randomByte(byte&);
        rcode compareHashValues(String&, String&);
        rcode sanitisationCheck(String&);
};

#endif