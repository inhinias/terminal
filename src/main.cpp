#include <Arduino.h>
#include <FS.h>
#include <SPIFFS.h>
#include "config.h"
#include "helper/logging.h"
#include "db/db.h"
#include "transaction/transaction.h"
#include "display/drinks.h"

//Global Classes
#if USING_DISPLAY == USE_SSD1306
    #include "display/menuOled.h"

    MenuOLED menu = MenuOLED();
#elif USING_DISPLAY == USE_ST7735_LCD
    #include "display/menuTft.h"

    MenuTFT menu = MenuTFT();
#endif

Transaction trs = Transaction();

//Prototypes
void writeToConsole(const String& text); //Add a new line to the Display Console

//Global vars
bool setupFailed = false;
uint16_t showCreditCounter = 0;
uint8_t readDelay = 0;
bool isPauschal = true;

typedef struct{
    bool back = 0;
    bool prev = 0;
    bool next = 0;
    bool enter = 0;
    bool oldBack = 0;
    bool oldPrev = 0;
    bool oldNext = 0;
    bool oldEnter = 0;
}btns;


void setup() {
    Serial.begin(115200);		// Initialize serial communications with the PC
    while (!Serial);		// Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)

    //Print out basic info
    log("Starting Reymli Terminal!", logINFO);
    log("Version: " SW_VERSION, logINFO);
    log("HW Version: " HW_VERSION, logINFO);
    log("HW Variant: " HW_VARIANT, logINFO);

    rcode c;
    #if USING_DISPLAY == USE_SSD1306
        c = menu.init();
    #elif USING_DISPLAY == USE_ST7735_LCD
        c = menu.init();
    #endif

    if(c.code != eOK){
        log(c.returnMessage, logERROR);
        log("Could not initialize Display class", logERROR);
        setupFailed = true;
        return;
    }

    //Check if the transaction class was initialized
    c = trs.init();
    if(c.code != eOK){
        log(c.returnMessage, logERROR);
        log("Could not initialize Transaction class", logERROR);
        setupFailed = true;
        return;
    }

    //Use buttons input
    pinMode(BTN_PREV, INPUT);
    pinMode(BTN_NEXT, INPUT);
    pinMode(BTN_BACK, INPUT);
    pinMode(BTN_ENTER, INPUT);
    pinMode(LED_STATUS, OUTPUT);
    digitalWrite(LED_STATUS, HIGH);

    ledcSetup(0, PWM_FREQ, 8);
    ledcAttachPin(BUZZER, 0);

    isPauschal = !digitalRead(BTN_ENTER);

    if(isPauschal){
        menu.open(eMenuPauschal);
    }
    else{
        menu.open(eMenuDrinkType, 1);
    }

    DB database = DB();
    database.init();
    //SPIFFS.format();
}

//uint8_t specialBlocks[50] = {0,1,2,3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63};

void loop() {
    EMenu currentMenu = menu.getCurrentMenu();
    EDrinksType currentDrinkType = menu.getCurrentDrinkType();
    SDrink currentDrink = menu.getCurrentDrink();
    uint16_t currentItemIndex = menu.getCurrentItemIndex();
    String uid; // String to store the card UID
    rcode c;

    Drinks drink = Drinks();
    std::vector<SDrink> drinksOfType = drink.getDrinksForType(currentDrinkType);
    uint16_t currentDrinkIndex = drink.getDrinkIndexInItsTypeList(currentDrink);

    //Invert cause pullup and do pos edge detection
    static btns buttons;
    buttons.back = (digitalRead(BTN_BACK)) && !buttons.oldBack;
    buttons.prev = (digitalRead(BTN_PREV)) && !buttons.oldPrev;
    buttons.next = (digitalRead(BTN_NEXT)) && !buttons.oldNext;
    buttons.enter = (digitalRead(BTN_ENTER)) && !buttons.oldEnter;

    buttons.oldBack  = digitalRead(BTN_BACK);
    buttons.oldPrev  = digitalRead(BTN_PREV);
    buttons.oldNext  = digitalRead(BTN_NEXT);
    buttons.oldEnter = digitalRead(BTN_ENTER);

    if(currentMenu == eMenuCredit || currentMenu == eMenuNotEnoughCredit || currentMenu == eMenuPaid){
        if(showCreditCounter < MENU_HOLD_TIME){
            if(showCreditCounter % BZZ_DIVIDER == 0 && showCreditCounter < BZZ_ON_TIME){
                ledcWrite(0, 128);
            }
            else{
                ledcWrite(0, 0);
            }
            showCreditCounter++;
        }
        else{
            ledcWrite(0, 0);
            showCreditCounter = 0;
            if(isPauschal){
                menu.open(eMenuPauschal);
            }
            else{
                menu.open(eMenuDrinkType, 1);
            }
        }
    }
    else{
        if(isPauschal){
            if(buttons.back){
                showCreditCounter = 0;
                menu.open(eMenuPauschal);
                currentMenu = eMenuPauschal;
            }

            if(currentMenu == eMenuAskCard){
                log("is asking card");
                if(buttons.back){
                    menu.open(eMenuPauschal);
                }
                else{
                    if(trs.newCardPresent(uid)){
                        DB database = DB();
                        database.init();
                        uint16_t netWorth;
                        netWorth = 0;
                        database.getUserNetWorth(uid, netWorth);
                        menu.open(eMenuCredit, netWorth);
                    }
                }
            }
            else if(currentMenu == eMenuMaster){
                log("is master");
                if(buttons.enter){
                    menu.open(eMenuDeposit, currentItemIndex);
                }
                else if(buttons.next && currentItemIndex < 64000){ //Allow a max of 640 chf to be deposited at once so an overflow can be evaded
                    currentItemIndex += MASTER_PRICE_INCREMENT;
                    log((String)currentItemIndex, logDEBUG);
                    menu.open(eMenuMaster, currentItemIndex);
                }
                else if(buttons.prev && currentItemIndex >= MASTER_PRICE_INCREMENT){ //Drinks are zero indexed this after index one has passed stop
                    currentItemIndex -= MASTER_PRICE_INCREMENT;
                    log((String)currentItemIndex, logDEBUG);
                    menu.open(eMenuMaster, currentItemIndex);
                }
                else{
                    menu.open(eMenuMaster, currentItemIndex);
                }
            }
            else if(currentMenu == eMenuDeposit){
                menu.open(eMenuDeposit, currentItemIndex);
                if(trs.newCardPresent(uid)){
                    readDelay = READ_DELAY;
                    //Do transaction
                    tTransaction transact;
                    transact.ammount = currentItemIndex;
                    transact.isPayment = false;
                    transact.transactorCardID = uid;

                    c = trs.makeTransaction(transact);
                    if(c.code != eOK){
                        log("Transaction failed", logERROR);
                    }

                    String transactString = "Deposited ";
                    transactString += (String)currentItemIndex;
                    transactString += " on card ";
                    transactString += uid;
                    log(transactString, logDEBUG);
                    menu.open(eMenuDrinkType, 1);
                }
            }
            else{
                //log("is pauschal");
                if(buttons.enter){
                    menu.open(eMenuAskCard);
                }
                else{
                    menu.open(eMenuPauschal);
                    if(trs.newCardPresent(uid)){
                        if(uid == MASTER_CARD_UID){
                            log("Master Card Detected!", logINFO);
                            currentItemIndex = 0;
                            menu.open(eMenuMaster, 0);
                        }
                        else{
                            readDelay = READ_DELAY;
                            //Do transaction

                            DB database = DB();
                            database.init();
                            uint16_t netWorth;
                            netWorth = 0;
                            database.getUserNetWorth(uid, netWorth);

                            //Cause comparing in the if is not possible cause it unsigned
                            int16_t calcWorth = (netWorth - PAUSCHAL_PREIS * 100);

                            //Transact only if enough money is present
                            if(calcWorth >= 0){
                                tTransaction transact;
                                transact.ammount = PAUSCHAL_PREIS * 100;
                                transact.isPayment = true;
                                transact.transactorCardID = uid;

                                c = trs.makeTransaction(transact);
                                if(c.code != eOK){
                                    log("Transaction failed", logERROR);
                                }

                                String transactString = "Deducted ";
                                transactString += (String)PAUSCHAL_PREIS;
                                transactString += ".00.- from card ";
                                transactString += uid;
                                log(transactString, logDEBUG);
                                menu.open(eMenuPaid);
                            }
                            else{
                                menu.open(eMenuNotEnoughCredit);
                            }
                        }
                    }
                }
            }
        }
        else{
            if(buttons.back){
                showCreditCounter = 0;
                menu.open(eMenuDrinkType, 1);
                currentMenu = eMenuDrinkType;
            }
            switch(currentMenu){
                case eMenuDrinkType:
                    if(buttons.enter){
                        menu.open(eMenuDrinks, 0);
                    }

                    //The eNUMBER_DRINK_TYPES also counts as a enum element thus dont count it
                    else if(buttons.next && ((eNUMBER_DRINK_TYPES - 1) > currentDrinkType)){
                        menu.open(eMenuDrinkType, (currentDrinkType + 1));
                    }
                    else if(buttons.prev && currentDrinkType > 1){ //There is a null menu Element so 2 is the last valid index
                        menu.open(eMenuDrinkType, (currentDrinkType - 1));
                    }
                    else{
                        /*if(trs.newCardPresent(uid)){
                            if(uid != MASTER_CARD_UID){
                                menu.open(eMenuCredit, 0);
                            }
                        }*/
                    }
                    break;
                case eMenuDrinks:
                    if(buttons.enter){
                        menu.open(eMenuTransact, currentDrink.price);
                    }
                    else if(buttons.next && ((drinksOfType.size()-1) > currentDrinkIndex)){ //Zero index requires size to be one smaller
                        log((String)currentDrinkIndex, logDEBUG);
                        currentDrinkIndex++;
                        menu.open(eMenuDrinks, (currentDrinkIndex));
                    }
                    else if(buttons.prev && currentDrinkIndex >= 1){ //Drinks are zero indexed this after index one has passed stop
                        currentDrinkIndex--;
                        menu.open(eMenuDrinks, (currentDrinkIndex));
                    }
                    else{
                        /*if(trs.newCardPresent(uid)){
                            if(uid != MASTER_CARD_UID){
                                menu.open(eMenuCredit, 0);
                            }
                        }*/
                    }
                    break;
                case eMenuKeypad:
                    break;
                case eMenuMaster:
                    if(buttons.enter){
                        menu.open(eMenuDeposit, currentItemIndex);
                    }
                    else if(buttons.next && currentItemIndex < 64000){ //Allow a max of 640 chf to be deposited at once so an overflow can be evaded
                        currentItemIndex += MASTER_PRICE_INCREMENT;
                        log((String)currentItemIndex, logDEBUG);
                        menu.open(eMenuMaster, currentItemIndex);
                    }
                    else if(buttons.prev && currentItemIndex >= MASTER_PRICE_INCREMENT){ //Drinks are zero indexed this after index one has passed stop
                        currentItemIndex -= MASTER_PRICE_INCREMENT;
                        log((String)currentItemIndex, logDEBUG);
                        menu.open(eMenuMaster, currentItemIndex);
                    }
                    else{
                        menu.open(eMenuMaster, currentItemIndex);
                    }
                    break;
                case eMenuTransact:
                    menu.open(eMenuTransact, currentDrink.price);
                    if(trs.newCardPresent(uid)){
                        if(uid != MASTER_CARD_UID){
                            readDelay = READ_DELAY;
                            //Do transaction

                            DB database = DB();
                            database.init();
                            uint16_t netWorth;
                            netWorth = 0;
                            database.getUserNetWorth(uid, netWorth);

                            //Transact only if enough money is present
                            if((netWorth - currentDrink.price) >= 0){
                                tTransaction transact;
                                transact.ammount = currentDrink.price;
                                transact.isPayment = true;
                                transact.transactorCardID = uid;

                                c = trs.makeTransaction(transact);
                                if(c.code != eOK){
                                    log("Transaction failed", logERROR);
                                }

                                String transactString = "Deducted ";
                                transactString += (String)currentDrink.price;
                                transactString += " from card ";
                                transactString += uid;
                                log(transactString, logDEBUG);
                                menu.open(eMenuDrinkType, 1);
                            }
                            else{
                                menu.open(eMenuNotEnoughCredit);
                            }
                        }
                    }
                    break;

                case eMenuDeposit:
                    menu.open(eMenuDeposit, currentItemIndex);
                    if(trs.newCardPresent(uid)){
                        readDelay = READ_DELAY;
                        //Do transaction
                        tTransaction transact;
                        transact.ammount = currentItemIndex;
                        transact.isPayment = false;
                        transact.transactorCardID = uid;

                        c = trs.makeTransaction(transact);
                        if(c.code != eOK){
                            log("Transaction failed", logERROR);
                        }

                        String transactString = "Deposited ";
                        transactString += (String)currentItemIndex;
                        transactString += " on card ";
                        transactString += uid;
                        log(transactString, logDEBUG);
                        menu.open(eMenuDrinkType, 1);
                    }
                    break;

                case eMenuCredit:
                    break;
                default:
                    break;
            }

            //Dont read any cards when in any of those menus (they read them on their own)
            if(!(currentMenu == eMenuCredit || currentMenu == eMenuDeposit || currentMenu == eMenuTransact)){
                //Wait for 2 sek when a transaction was made.
                if(readDelay == 0){
                    if(trs.newCardPresent(uid)){
                        if(uid == MASTER_CARD_UID){
                            log("Master Card Detected!", logINFO);
                            currentItemIndex = 0;
                            menu.open(eMenuMaster, currentItemIndex);
                        }
                        else{
                            DB database = DB();
                            database.init();
                            uint16_t netWorth;
                            netWorth = 0;
                            database.getUserNetWorth(uid, netWorth);
                            menu.open(eMenuCredit, netWorth);
                        }
                    }
                }
                else{
                    readDelay--;
                }
            }
        }
    }

    delay(10); //Debounce delay and
}