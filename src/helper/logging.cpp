#include "helper/logging.h"
#include "config.h"

eLogLevel currentLogLevel = DEFAULT_LOG_LEVEL;

void appendToLog(String logText, eLogLevel logLevel);
void writeToLog(String logText);
String getLogLevelName(eLogLevel);

/***************************************************************************************************
                    Public Methods
****************************************************************************************************/
void log(String logText){
    appendToLog(logText, currentLogLevel);
}

void log(rcode returnCodeForLog){
    if(returnCodeForLog.code == OK && currentLogLevel <= logINFO) appendToLog(returnCodeForLog.returnMessage, logINFO);
    else if(returnCodeForLog.code == eGENERAL_WARN && currentLogLevel <= logWARN) appendToLog(returnCodeForLog.returnMessage, logWARN);
    else appendToLog(returnCodeForLog.returnMessage, logERROR);
}

void log(String logText, eLogLevel stringLogLevel){
    if(stringLogLevel >= currentLogLevel) appendToLog(logText, stringLogLevel);
}

void setlogLevel(eLogLevel newLogLevel){
    currentLogLevel = newLogLevel;
}

eLogLevel getLogLevel(){
    return currentLogLevel;
}


/***************************************************************************************************
                    Private Methods
****************************************************************************************************/
void appendToLog(String logText, eLogLevel logLevel){
    String logTextFormatted = "";
    //logTextFormatted += Time::currentTime();
    logTextFormatted += "2020-07-09 00:00:00 ";
    logTextFormatted += getLogLevelName(logLevel);
    logTextFormatted += " ";
    logTextFormatted += logText;

    writeToLog(logTextFormatted);
}

void writeToLog(String logText){
    Serial.println(logText);
}

String getLogLevelName(eLogLevel logLevel){
    switch (logLevel){
        case logERROR:
            return "ERROR";
            break;
        case logWARN:
            return "WARN";
            break;
        case logINFO:
            return "INFO";
            break;
        case logDEBUG:
            return "DEBUG";
            break;
        default:
            return "ILLEGAL";
            break;
    }
}