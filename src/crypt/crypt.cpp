#include <pgmspace.h>
#include "crypt/crypt.h"
#include "config.h"
#include "helper/helper.h"

/***************************************************************************************************
                    Con and Destructors
****************************************************************************************************/
Crypt::Crypt(){

}

Crypt::~Crypt(){

}



/***************************************************************************************************
                    Public Methods
****************************************************************************************************/
rcode Crypt::hash(String stringToHash, String& hash){
    rcode c;
    
    #if HASH_ALGO == NO_HASH
        char tmp[ARRAY_LEN((stringToHash.c_str()))];
        strcpy(tmp, stringToHash.c_str());
        hash = tmp;

        c.code = eGENERAL_WARN;
        c.returnMessage = "Hashing is disabled in conf! Returning given String.";
    #elif HASH_ALGO == BLAKE2S
        BLAKE2s blake2s;
        c.code = GENERAL_FAULT;
        c.returnMessage = "Not Implemented";
    #elif HASH_ALGO == BLAKE2B
        BLAKE2b blake2b;
        c.code = GENERAL_FAULT;
        c.returnMessage = "Not Implemented";
    #elif HASH_ALGO == BLAKE3
        BLAKE2b blake2b;
        c.code = GENERAL_FAULT;
        c.returnMessage = "Not Implemented";
    #elif HASH_ALGO == SHA3_512
        c.code = GENERAL_FAULT;
        c.returnMessage = "Not Implemented";
    #elif HASH_ALGO == SHA3_256
        c.code = GENERAL_FAULT;
        c.returnMessage = "Not Implemented";
    #else
        c.code = GENERAL_FAULT;
        c.returnMessage = "Not Implemented";
    #endif

    return c;
}

rcode Crypt::encrypt(String, String&){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

rcode Crypt::randomByte(byte&){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

rcode Crypt::compareHashValues(String& val1, String& val2){
    rcode c;
    if(strcmp(val1.c_str(), val2.c_str()) == 0){
        c.code = OK;
        c.returnMessage = "Strings Match";
    }
    else{
        c.code = eGENERAL_FAULT;
        c.returnMessage = "Hash values mismatch!";
    }
    return c;
}

rcode Crypt::sanitisationCheck(String& value){
    rcode c;
    c.code = OK;
    c.returnMessage = "String is clean!";
    String checkValues[] = {"DROP", "SELECT"};

    for(uint8_t i = 0; i<ARRAY_LEN(checkValues); i++){
        if(strstr(value.c_str(), checkValues[i].c_str()) != nullptr){
            c.code = eGENERAL_FAULT;
            c.returnMessage = "Possible SQL injection?";
            break;
        }
    }

    return c;
}