#include <SPI.h>
#include "db/db.h"
#include "helper/logging.h"
#include "db/fileSystem.h"
#include "config.h"

DB::DB(){

}

DB::~DB(){

}

rcode DB::init(){
    rcode c;
    c.code = eOK;
    log("init DB", logDEBUG);

    #if FS_LOCATION == USE_SPIFFS
        #if FORMAT_SPIFFS
            SPIFFS.format();
        #endif
        if(!SPIFFS.begin()){
            c.code = eGENERAL_FAULT;
            c.returnMessage = "Unable to open filesystem!";
            log(c.returnMessage, logERROR);
            return c;
        }

        File root = SPIFFS.open("/");
        if(!root || !root.isDirectory()){
            c.code = eGENERAL_FAULT;
            c.returnMessage = "Unable to access root dir!";
            log(c.returnMessage, logERROR);
            return c;
        }
        else{
            log("SPIFFS now available", logDEBUG);
        }

        #if SOFTWARE_STAGE == STAGE_DEBUG
            File file = root.openNextFile();
            while (file) {
                String pathText; 
                if (file.isDirectory()){
                    pathText = "DIR: ";
                    pathText += file.name();
                }
                else{
                    pathText = "FILE: ";
                    pathText += file.name();
                    pathText += " SIZE: ";
                    pathText += file.size();
                }
                log(pathText);
                while(file.available()){
                    Serial.write(file.read());
                }
                file = root.openNextFile();
            }
        #endif
    #elif FS_LOCATION == USE_SD_CARD
        SPIClass spiSD(HSPI);
        if(!SD.begin(SD_CS, spiSD)){
            c.code = eGENERAL_FAULT;
            c.returnMessage = "Unable to open filesystem!";
            log(c.returnMessage, logERROR);
            return c;
        }

        sdcard_type_t cardType = SD_MMC.cardType();
        switch (cardType){
            case CARD_NONE:
                c.code = eGENERAL_FAULT;
                c.returnMessage = "No SD-Card Detected";
                log(c.returnMessage, logERROR);
                return c;
                break;

            case CARD_MMC:
                log("Card Type: MMC", logINFO);
            case CARD_SD:
                log("Card Type: SDSC", logINFO);
            case CARD_SDHC:
                log("Card Type: SDHC", logINFO);
            default:
                log("unknown card type", logWARN);
                break;
        }

        uint32_t cardFreeSpace = (SD_MMC.totalBytes())/(1024 * 1024);
        log("Free Space" + (String)cardFreeSpace, logINFO);
        uint32_t cardUsedSpace = (SD_MMC.usedBytes())/(1024 * 1024);
        log("Used Space" + (String)cardUsedSpace, logINFO);

        log("SD-Card now available", logDEBUG);
    #endif


    c = createTables();

    if(c.code != eOK){
        log("Failed to create tables", logERROR);
    }
    else{
        log("Database now available", logDEBUG);
    }
    return c;
}



rcode DB::getTransactionByID(String transId, tTransaction& transact){
    std::vector<tTransaction> allTransactions;
    rcode c;

    c = getAllTransactions(allTransactions);
    for(uint16_t i=0; i < allTransactions.size(); i++){
        if(allTransactions[i].transactionID == transId){
            transact = allTransactions[i];
            c.code = eOK;
            return c;
        }
    }

    c.code = eGENERAL_WARN;
    c.returnMessage = "Transaction not found";
    return c;
}



rcode DB::getUserTransactions(String uId, std::vector<tTransaction>& transactions){
    std::vector<tTransaction> allTransactions;
    rcode c;

    c = getAllTransactions(allTransactions);
    if(c.code != eOK){
        log("getting trans failed", logERROR);
        return c;
    }
    else{
        for(uint16_t i=0; i < allTransactions.size(); i++){
            if(allTransactions[i].transactorCardID == uId){
                transactions.push_back(allTransactions[i]);
            }
        }   
    }

    c.code = eOK;
    return c;
}

rcode DB::getUserNetWorth(String uId, uint16_t& netWorth){
    std::vector<tTransaction> allTransactions;
    rcode c;
    netWorth = 0;

    c = getUserTransactions(uId, allTransactions);
    if(c.code != eOK){
        return c;
    }
    else{
        for(uint16_t i=0; i < allTransactions.size(); i++){
            if(allTransactions[i].isPayment){
                netWorth -= allTransactions[i].ammount;
            }
            else{
                netWorth += allTransactions[i].ammount;
            }
        }
    }

    c.code = eOK;
    return c;
}



rcode DB::getUserIDByCardId(String, tUser&){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}



rcode DB::insertTransaction(tTransaction& trans){
    rcode c;

    #if FS_LOCATION == USE_SPIFFS
        File transFile = SPIFFS.open(TRANSACTIONS_FILE, "a");  //Open transactions to append
    #elif FS_LOCATION == USE_SD_CARD
        File transFile = SD.open(TRANSACTIONS_FILE, "a");
    #endif

    String transData;
    transData = trans.transactionID;
    transData += ";";
    transData += trans.transactorCardID;
    transData +=  ";";
    transData += trans.ammount;
    transData += ";";
    transData += trans.isPayment;

    transFile.println(transData);
    transFile.close();

    c.code = eOK;
    return c;
}



rcode DB::updateTransaction(tTransaction&){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}



rcode DB::deleteTransaction(tTransaction&){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}



rcode DB::createTables(){
    rcode c;
    c.code = eOK;

    /*
    * Create Transactions table
    * id = Primary Key
    * transactorCardId = id the transactor card has in the users table
    * ammount = the ammount of money transferred
    * isPayment = is 1 when the ammount transferred is being paid (In terms of the user)
    */
    File currentTable;
    #if FS_LOCATION == USE_SPIFFS
    if(!SPIFFS.exists(TRANSACTIONS_FILE)){
        log("creatin transaction file");
        currentTable = SPIFFS.open(TRANSACTIONS_FILE, "w");  //Create the db file if it doesnt exist
    #elif FS_LOCATION == USE_SD_CARD
    if(!(SD.exists(TRANSACTIONS_FILE))){
        currentTable = SD.open(TRANSACTIONS_FILE, "w");  //Create the db file if it doesnt exist
    #endif
        if(!currentTable){
            c.code = eFILE_IO_ERROR;
            c.returnMessage = "Failed to create file: ";
            c.returnMessage += TRANSACTIONS_FILE;
            currentTable.close();
            return c;
        }
        else{
            currentTable.println("id;transactorCardId;ammount;isPayment");
        }
        currentTable.close();
    }

    /*
    * Create users table
    * id = Primary Key
    * username = The real name of the user
    * cardId = The Current card id the user has
    */
    #if FS_LOCATION == USE_SPIFFS
    if(!SPIFFS.exists(USERS_FILE)){
        currentTable = SPIFFS.open(USERS_FILE, "w");  //Create the db file if it doesnt exist
    #elif FS_LOCATION == USE_SD_CARD
    if(!(SD.exists(TRANSACTIONS_FILE))){
        currentTable = SD.open(USERS_FILE, "w");  //Create the db file if it doesnt exist
    #endif
        if (!currentTable){
            c.code = eFILE_IO_ERROR;
            c.returnMessage = "Failed to create file: ";
            c.returnMessage += USERS_FILE;
            currentTable.close();
            return c;
        }
        else{
            currentTable.println("id;username;cardId");
        }
    }
    currentTable.close();

    return c;
}



//stackoverflow.com/questions/12911299
rcode DB::getField(String line, uint16_t index, String& token){
    rcode c;
    c.code = eOK;

    std::vector<String> tokens;
    getFields(line, tokens);

    if(tokens.size() < index){
        c.code = eGENERAL_WARN;
        c.returnMessage = "The index given is out of bounds for the list";
        log(c.returnMessage, logWARN);
    }
    else{
        token = tokens[index];
    }
    return c;
}



rcode DB::getFields(String line, std::vector<String>& tokens){
    rcode c;

    if((line == NULL) || line.isEmpty()){
        c.code = eGENERAL_WARN;
        c.returnMessage = "Empty String given to getFields()";
        log(c.returnMessage, logWARN);
        return c;
    }

    //Gotta use std::string as i provides more functions
    //but cant use it everywhere as it causes problems on the Arduino side, what?
    std::string s = line.c_str();
    std::string delimiter = ";";

    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        tokens.push_back(token.c_str());
        s.erase(0, pos + delimiter.length());
    }
    tokens.push_back(s.c_str()); //Theres an element after the last semicolon add it!

    c.code = eOK;
    return c;
}



rcode DB::fieldToTransaction(String line, tTransaction& trans){
    std::vector<String> transData;
    rcode c;
    c.code = eOK;

    c = getFields(line, transData); //Get a String vector of all fields in the given line
    if(c.code != eOK){
        return c;
    }

    //Populate transaction struct with read data
    trans.transactionID = transData[0];
    trans.transactorCardID = transData[1];
    trans.ammount = atoi(transData[2].c_str());

    //Compare as integers as String with const char* comparison is wildly unreliable
    if(atoi(transData[3].c_str()) >= 1){
        trans.isPayment = true;
    }
    else{
        trans.isPayment = false;
    }

    return c;
}



rcode DB::fieldToUser(String line, tUser& user){
    std::vector<String> userData;
    rcode c;
    c.code = eOK;

    c = getFields(line, userData); //Get a String vector of all fields in the given line
    if(c.code != eOK){
        return c;
    }

    //Populate transaction struct with read data
    user.transactorID = userData[0];
    user.username = userData[1];
    user.cardId = userData[2];

    return c;
}



rcode DB::getAllTransactions(std::vector<tTransaction>& allTrans){
    rcode c;

    #if FS_LOCATION == USE_SPIFFS
        File transactFile = SPIFFS.open(TRANSACTIONS_FILE, "r");
    #elif FS_LOCATION == USE_SD_CARD
        File transactFile = SD.open(TRANSACTIONS_FILE, "r");
    #endif

    //read the first line of the csv (Column identifiers)
    String currentReadTrans = transactFile.readStringUntil('\n');

    //Loop through the entire csv file and store all elements in a vector
    while(transactFile.position() < transactFile.size()){
        String strCurrentReadTrans = transactFile.readStringUntil('\n');

        tTransaction currentTrans;
        c = fieldToTransaction(strCurrentReadTrans, currentTrans); //Get the currently read Transaction

        //Error handling
        if(c.code != eOK){
            return c;
        }
        else{
            allTrans.push_back(currentTrans); //Append the currently read transaction to the transactions list
        }
    }

    transactFile.close();

    c.code = eOK;
    return c;
}



rcode DB::getAllUsers(std::vector<tUser>& allUsers){
    rcode c;

    #if FS_LOCATION == USE_SPIFFS
        File transactFile = SPIFFS.open(USERS_FILE, "r");
    #elif FS_LOCATION == USE_SD_CARD
        File transactFile = SD.open(USERS_FILE, "r");
    #endif

    //read the first line of the csv (Column identifiers)
    String currentReadTrans = transactFile.readStringUntil('\n');
    //Serial.println(currentReadTrans);

    //Loop through the entire csv file and store all elements in a vector
    while(transactFile.position() < transactFile.size()){
        String strCurrentReadUser = transactFile.readStringUntil('\n');

        //getField needs a char* but someString.c_str() returns a const char *
        char *currentReadUser = new char[strCurrentReadUser.length() + 1];
        strcpy(currentReadUser, strCurrentReadUser.c_str());


        tUser currentUser;
        c = fieldToUser(currentReadUser, currentUser); //Get the currently read Transaction

        //Error handling
        if(c.code != eOK){
            free(currentReadUser); //Get rid of the char* copy of the constCurrentReadTrans
            return c;
        }
        else{
            allUsers.push_back(currentUser); //Append the currently read transaction to the transactions list
        }

        free(currentReadUser); //Get rid of the char* copy of the constCurrentReadTrans
    } 

    transactFile.close();

    c.code = eOK;
    return c;
}