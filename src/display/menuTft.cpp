#include "display/menuTft.h"
#include "display/drinks.h"


MenuTFT::MenuTFT(){
    TFT_eSPI dsp = TFT_eSPI();

    currentDrinkType = eBier;
}
MenuTFT::~MenuTFT(){

}

rcode MenuTFT::init(){
    rcode c;
    c.code = eOK;

    //Init TFT
    dsp.init();
    dsp.setRotation(3);
    dsp.fillScreen(BLACK);
    dsp.setTextColor(WHITE);
    dsp.setTextSize(1);

    resetDisplay();

    log("TFT Initialized", logDEBUG);
    return c;
}


rcode MenuTFT::open(EMenu menuIndex){
    rcode c;
    c.code = eOK;

    if(currentMenu != menuIndex){
        currentMenu = menuIndex;
        resetDisplay();
        if(menuIndex == eMenuNotEnoughCredit){
            dsp.setTextColor(BLACK);
            uint8_t widthCenter = dsp.drawString("Nicht genug Guthaben!", 0, 0, 2);
            dsp.fillScreen(BLACK);
            dsp.setTextColor(WHITE);
            dsp.drawString("Nicht genug Guthaben!", (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
        }
        else if(menuIndex == eMenuPauschal){
            dsp.setTextColor(BLACK);
            uint8_t widthCenter = dsp.drawString("Pauschal", 0, 0, 4);
            uint8_t widthPrice = dsp.drawString(((String)PAUSCHAL_PREIS) + ".00.-", 0, 0, 4);
            dsp.fillScreen(BLACK);

            dsp.setTextColor(WHITE);
            dsp.drawString("Pauschal", (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 4);

            dsp.drawTriangle( 0, FONT_HEIGHT, 
                                ARROW_WIDTH, FONT_HEIGHT + ARROW_WIDTH/2,
                                0, FONT_HEIGHT + ARROW_WIDTH,
                                TFT_GREEN);

            dsp.drawTriangle( LCD_WIDTH -1 , FONT_HEIGHT, 
                                LCD_WIDTH - ARROW_WIDTH, FONT_HEIGHT + ARROW_WIDTH/2,
                                LCD_WIDTH - 1, FONT_HEIGHT + ARROW_WIDTH,
                                TFT_GREEN);

            dsp.drawString(((String)PAUSCHAL_PREIS) + ".00.-", (LCD_WIDTH - widthPrice)/2, LCD_HEIGHT - FONT_HEIGHT - BOTTOM_OFFSET, 4);
        }
        else if(menuIndex == eMenuAskCard){
            dsp.setTextColor(BLACK);
            uint8_t widthCenter = dsp.drawString("Karte Bitte...", 0, 0, 2);
            dsp.fillScreen(BLACK);
            dsp.setTextColor(WHITE);
            dsp.drawString("Karte Bitte...", (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
        }
        else if(menuIndex == eMenuPaid){
            dsp.setTextColor(BLACK);
            uint8_t widthCenter = dsp.drawString("3.00.- Verbucht", 0, 0, 2);
            dsp.fillScreen(BLACK);
            dsp.setTextColor(WHITE);
            dsp.drawString("3.00.- Verbucht", (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
        }
        else{
            dsp.drawString(drinkTypeToText(currentDrinkType), 0, FONT_HEIGHT, 4);
        }
    }

    return c;
}


rcode MenuTFT::open(EMenu menuIndex, uint16_t itemIndex){
    rcode c;
    c.code = eOK;

    if(currentMenu != menuIndex || currentItemIndex != itemIndex){
        resetDisplay();
        currentMenu = menuIndex;
        currentItemIndex = itemIndex;
        Drinks drink = Drinks();
        std::vector<SDrink> drinksOfType = drink.getDrinksForType(currentDrinkType);
        String price = (String)(drinksOfType[itemIndex].price / 100);
        price += ".00.-";


        uint16_t widthCenter = 0;
        uint16_t widthPrice = 0;
        bool smallFont = false;

        switch(menuIndex){
            case eMenuDrinkType:
                currentDrinkType = (EDrinksType)itemIndex;

                dsp.setTextColor(BLACK);
                widthCenter = dsp.drawString(drinkTypeToText(currentDrinkType), 0, 0, 4);
                if(widthCenter > MAX_TEXT_WIDTH){
                    smallFont = true;
                    widthCenter = dsp.drawString(drinkTypeToText(currentDrinkType), 0, 0, 2);
                }
                dsp.fillScreen(BLACK);

                dsp.setTextColor(WHITE);
                if(smallFont){
                    dsp.drawString(drinkTypeToText(currentDrinkType), (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
                }
                else{
                    dsp.drawString(drinkTypeToText(currentDrinkType), (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 4);
                }

                if(currentDrinkType > eBier){
                    dsp.drawTriangle( ARROW_WIDTH, FONT_HEIGHT, 
                                      0, FONT_HEIGHT + ARROW_HEIGHT/2,
                                      ARROW_WIDTH, FONT_HEIGHT + ARROW_HEIGHT,
                                      TFT_GREEN);
                }
                if(currentDrinkType < eAlcFree){
                    dsp.drawTriangle( LCD_WIDTH - ARROW_WIDTH, FONT_HEIGHT, 
                                      LCD_WIDTH - 1, FONT_HEIGHT + ARROW_HEIGHT/2,
                                      LCD_WIDTH - ARROW_WIDTH, FONT_HEIGHT + ARROW_HEIGHT,
                                      TFT_GREEN);
                }

                break;
            case eMenuDrinks:
                currentDrink = drinksOfType[itemIndex];

                dsp.setTextColor(BLACK);
                widthCenter = dsp.drawString(currentDrink.name, 0, 0, 4);
                if(widthCenter > MAX_TEXT_WIDTH){
                    smallFont = true;
                    widthCenter = dsp.drawString(currentDrink.name, 0, 0, 2);
                }
                widthPrice = dsp.drawString(price, 0, 0, 4);
                dsp.fillScreen(BLACK);

                dsp.setTextColor(WHITE);
                if(smallFont){
                    dsp.drawString(currentDrink.name, (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
                }
                else{
                    dsp.drawString(currentDrink.name, (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 4);
                }

                if(itemIndex > 0){
                    dsp.drawTriangle( ARROW_WIDTH, FONT_HEIGHT, 
                                      0, FONT_HEIGHT + ARROW_WIDTH/2,
                                      ARROW_WIDTH, FONT_HEIGHT + ARROW_WIDTH,
                                      TFT_GREEN);
                }
                if(itemIndex < drinksOfType.size()-1){
                    dsp.drawTriangle( LCD_WIDTH - ARROW_WIDTH, FONT_HEIGHT, 
                                      LCD_WIDTH - 1, FONT_HEIGHT + ARROW_WIDTH/2,
                                      LCD_WIDTH - ARROW_WIDTH, FONT_HEIGHT + ARROW_WIDTH,
                                      TFT_GREEN);
                }

                dsp.drawString(price, (LCD_WIDTH - widthPrice)/2, LCD_HEIGHT - FONT_HEIGHT - BOTTOM_OFFSET, 4);
                break;
            case eMenuMaster:
                dsp.setTextColor(BLACK);
                widthCenter = dsp.drawString(menuToText(currentMenu), 0, 0, 4);
                if(widthCenter > MAX_TEXT_WIDTH){
                    smallFont = true;
                    widthCenter = dsp.drawString(menuToText(currentMenu), 0, 0, 2);
                }
                widthPrice = dsp.drawString((String)(itemIndex / 100) + ".00.-", 0, FONT_HEIGHT, 4);
                dsp.fillScreen(BLACK);

                dsp.setTextColor(WHITE);
                if(smallFont){
                    dsp.drawString(menuToText(currentMenu), (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
                }
                else{
                    dsp.drawString(menuToText(currentMenu), (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 4);
                }

                dsp.drawString((String)(itemIndex / 100) + ".00.-", (LCD_WIDTH - widthPrice)/2, LCD_HEIGHT - FONT_HEIGHT - BOTTOM_OFFSET, 4);
                break;
            case eMenuKeypad:
                log("The keypad is not implemented yet?!", logWARN);
                break;
            case eMenuTransact:
            case eMenuDeposit:
            case eMenuCredit:
                dsp.setTextColor(BLACK);
                widthCenter = dsp.drawString((String)(itemIndex / 100) + ".00.-", 0, 0, 4);
                if(widthCenter > MAX_TEXT_WIDTH){
                    smallFont = true;
                    widthCenter = dsp.drawString((String)(itemIndex / 100) + ".00.-", 0, 0, 2);
                }
                dsp.fillScreen(BLACK);

                dsp.setTextColor(WHITE);
                if(smallFont){
                    dsp.drawString((String)(itemIndex / 100) + ".00.-", (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 2);
                }
                else{
                    dsp.drawString((String)(itemIndex / 100) + ".00.-", (LCD_WIDTH - widthCenter)/2, FONT_HEIGHT, 4);
                }
                break;
            default:
                break;
        }
    }

    return c;
}


EMenu MenuTFT::getCurrentMenu(){
    return currentMenu;
}


EDrinksType MenuTFT::getCurrentDrinkType(){
    return currentDrinkType;
}

SDrink MenuTFT::getCurrentDrink(){
    return currentDrink;
}

uint16_t MenuTFT::getCurrentItemIndex(){
    return currentItemIndex;
}

void MenuTFT::resetDisplay(){
    dsp.fillScreen(BLACK);
    dsp.setTextColor(WHITE, BLACK);
    //dsp.setTextSize(TFT_FONT_SIZE);
    //dsp.setCursor(0, 0);
}

String MenuTFT::menuToText(EMenu menu){
    switch(menu){
        case eNoMenu:
            return "NULL";
            break;
        case eMenuDrinkType:
            return "Drinks";
            break;
        case eMenuDrinks:
            return "Current Drink";
            break;
        case eMenuKeypad:
            return "Wert Eingeben";
            break;
        case eMenuMaster:
            return "Aufladen";
            break;
        case eMenuTransact:
            return "Betrag";
            break;
        default:
            return "Fault";
            break;
    }
}


void MenuTFT::writeToConsole(const String& text){
    static String onConsole[LCD_CONSOLE_LINES]; //Text that is currently displayed

    //Shift everythign back
    for(uint8_t i=0; i < LCD_CONSOLE_LINES; i++){
        onConsole[i] = onConsole[i+1];
    }
    onConsole[LCD_CONSOLE_LINES-1] = text; //Add the new text on the end

    dsp.fillScreen(BLACK); //Clear the screen
    for(uint8_t i=0; i < LCD_CONSOLE_LINES; i++){
        if(!(onConsole[i].isEmpty())) dsp.drawString(onConsole[i], 0, 26*i , 4); //Text, PosX, PosY (26=line height, i=line), font
    }
}