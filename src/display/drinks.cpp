#include "display/drinks.h"

Drinks::Drinks(){
    SDrink currentDrink;

    currentDrink.menu = eBier;
    currentDrink.price = 300;
    currentDrink.name = "Eichhof";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eBier;
    currentDrink.price = 400;
    currentDrink.name = "Smirnoff";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eBier;
    currentDrink.price = 300;
    currentDrink.name = "Radler";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eLongDrink;
    currentDrink.price = 400;
    currentDrink.name = "Vodka";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eLongDrink;
    currentDrink.price = 400;
    currentDrink.name = "Passoa";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 200;
    currentDrink.name = "Hirsch";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 300;
    currentDrink.name = "Flying Hirsch";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 200;
    currentDrink.name = "Willy Sauer";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 300;
    currentDrink.name = "Saure Zunge";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 300;
    currentDrink.name = "Waschmaschine";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 200;
    currentDrink.name = "XuXu";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eShot;
    currentDrink.price = 200;
    currentDrink.name = "Sex on the Beach";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eWein;
    currentDrink.price = 600;
    currentDrink.name = "Flasche Holunder";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eWein;
    currentDrink.price = 600;
    currentDrink.name = "Flasche Erbeer";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eWein;
    currentDrink.price = 0;
    currentDrink.name = "Div. Weisswein";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eKaffe;
    currentDrink.price = 400;
    currentDrink.name = "Moerdertee";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eKaffe;
    currentDrink.price = 300;
    currentDrink.name = "Kafi Schnaps";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eAlcFree;
    currentDrink.price = 200;
    currentDrink.name = "Cola";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eAlcFree;
    currentDrink.price = 200;
    currentDrink.name = "Citro";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eAlcFree;
    currentDrink.price = 200;
    currentDrink.name = "Energy";
    drinkList.push_back(currentDrink);

    currentDrink.menu = eAlcFree;
    currentDrink.price = 200;
    currentDrink.name = "Orangesaft";
    drinkList.push_back(currentDrink);
}
Drinks::~Drinks(){

}

std::vector<SDrink> Drinks::getDrinksForType(EDrinksType type){
    std::vector<SDrink> drinksOfType;

    for(uint8_t i = 0; i < drinkList.size(); i++){
        if(drinkList[i].menu == type){
            drinksOfType.push_back(drinkList[i]);
        }
    }

    return drinksOfType;
}

uint8_t Drinks::getDrinkIndexInItsTypeList(SDrink drink){
    std::vector<SDrink> drinkList = getDrinksForType(drink.menu);

    for(uint16_t i=0; i < drinkList.size(); i++){
        if(drinkList[i].name == drink.name){
            return i;
        }
    }
    return NULL;
}

std::vector<SDrink> Drinks::getAllDrinks(){
    return drinkList;
}

String drinkTypeToText(EDrinksType type){
    switch (type){
        case eBier:
            return "Bier";
            break;
        case eLongDrink:
            return "Long Drinks";
            break;
        case eShot:
            return "Shots";
            break;
        case eWein:
            return "Wein";
            break;
        case eKaffe:
            return "Kaffe";
            break;
        case eAlcFree:
            return "Alkoholfrei";
            break;
        default:
            return "";
            break;
    }
    return "";
}