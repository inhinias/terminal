#include <Wire.h>
#include "display/menuOled.h"
#include "display/drinks.h"

MenuOLED::MenuOLED(){
    delay(50);
    dsp = Adafruit_SSD1306(OLED_WIDTH, OLED_HEIGHT, &Wire, -1, 800000U, 400000U); //OLED has no reset line thus -1
    
    currentDrinkType = eBier;
}
MenuOLED::~MenuOLED(){

}

rcode MenuOLED::init(){
    rcode c;
    c.code = eOK;

    if(!dsp.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { //Address 0x3C for 128x32
        c.code = eGENERAL_FAULT;
        c.returnMessage = "Failed to open SSD1306 OLED";
        log(c.returnMessage, logERROR);
        return c;
    }

    resetDisplay();

    log("OLED Initialized and cleared", logDEBUG);
    return c;
}

rcode MenuOLED::open(EMenu menuIndex){
    rcode c;
    c.code = eOK;


    if(currentMenu != menuIndex){
        currentMenu = menuIndex;
        resetDisplay();
        if(menuIndex == eMenuNotEnoughCredit){
            dsp.println("Nicht genug Guthaben!");
            dsp.display();
        }
        else if(menuIndex == eMenuPauschal){
            dsp.write(16);
            dsp.write(" ");
            dsp.println("Pauschal");
            dsp.println(((String)PAUSCHAL_PREIS) + ".00.-");
            dsp.display();
        }
        else if(eMenuAskCard){
            dsp.write(16);
            dsp.write(" ");
            dsp.println("Karte Bitte...");
            dsp.display();
        }
        else if(menuIndex == eMenuPaid){
            dsp.write(16);
            dsp.write(" ");
            dsp.println("3.00.- Verbucht");
            dsp.display();
        }
        else{
            dsp.println(drinkTypeToText(currentDrinkType));
        }
    }

    return c;
}



rcode MenuOLED::open(EMenu menuIndex, uint16_t itemIndex){
    rcode c;
    c.code = eOK;

    if(currentMenu != menuIndex || currentItemIndex != itemIndex){
        resetDisplay();
        currentMenu = menuIndex;
        currentItemIndex = itemIndex;
        Drinks drink = Drinks();
        std::vector<SDrink> drinksOfType = drink.getDrinksForType(currentDrinkType);

        switch(menuIndex){
            case eMenuDrinkType:
                currentDrinkType = (EDrinksType)itemIndex;
                dsp.write(16);
                dsp.println(menuToText(currentMenu));
                dsp.write(" ");
                dsp.write(26);
                dsp.println(drinkTypeToText(currentDrinkType));
                dsp.display();
                break;
            case eMenuDrinks:
                dsp.write(16);
                dsp.println(drinkTypeToText(currentDrinkType));
                dsp.write(" ");
                dsp.write(26);
                currentDrink = drinksOfType[itemIndex];
                dsp.println(drinksOfType[itemIndex].name);
                dsp.print((drinksOfType[itemIndex].price / 100));
                dsp.println(".00 CHF");
                dsp.display();
                break;
            case eMenuMaster:
                dsp.write(26);
                dsp.println(menuToText(currentMenu));
                dsp.print((String)(itemIndex / 100));
                dsp.println(".00 CHF");
                dsp.display();
                break;
            case eMenuKeypad:
                dsp.write(26);
                dsp.println(menuToText(currentMenu));
                dsp.println((String)itemIndex);
                dsp.display();
                break;
            case eMenuTransact:
                dsp.setCursor(0, 12);
                dsp.setTextSize(2);
                dsp.write(16);
                dsp.write(" ");
                dsp.print((String)(itemIndex / 100));
                dsp.println(".00.-");
                dsp.display();
                dsp.setTextSize(1);
                break;
            case eMenuDeposit:
                dsp.setCursor(0, 12);
                dsp.setTextSize(2);
                dsp.write(16);
                dsp.write(" ");
                dsp.print((String)(itemIndex / 100));
                dsp.println(".00.-");
                dsp.display();
                dsp.setTextSize(1);
                break;
            case eMenuCredit:
                dsp.setCursor(0, 12);
                dsp.setTextSize(2);
                dsp.write(16);
                dsp.write(" ");
                dsp.print((String)(itemIndex / 100));
                dsp.println(".00.-");
                dsp.display();
                dsp.setTextSize(1);
                break;
            default:
                break;
        }
    }

    return c;
}


EMenu MenuOLED::getCurrentMenu(){
    return currentMenu;
}


EDrinksType MenuOLED::getCurrentDrinkType(){
    return currentDrinkType;
}

SDrink MenuOLED::getCurrentDrink(){
    return currentDrink;
}

uint16_t MenuOLED::getCurrentItemIndex(){
    return currentItemIndex;
}


void MenuOLED::writeToConsole(const String& text){
    static String onConsole[OLED_CONSOLE_LINES]; //Text that is currently displayed

    //Shift everythign back
    for(uint8_t i=0; i < OLED_CONSOLE_LINES; i++){
        onConsole[i] = onConsole[i+1];
    }
    onConsole[OLED_CONSOLE_LINES-1] = text; //Add the new text on the end

    dsp.setTextSize(OLED_FONT_SIZE);
    dsp.setTextColor(SSD1306_WHITE);
    dsp.setCursor(0, 0);

    dsp.clearDisplay(); //Clear the screen
    for(uint8_t i=0; i < OLED_CONSOLE_LINES; i++){
        if(!(onConsole[i].isEmpty())) dsp.println(onConsole[i]);
    }
    dsp.display();
}

void MenuOLED::resetDisplay(){
    dsp.clearDisplay();
    dsp.setTextSize(OLED_FONT_SIZE);
    dsp.setTextColor(SSD1306_WHITE);
    dsp.setCursor(0, 0);
    dsp.cp437(true); //Use full 256 char 'Code Page 437' font
    dsp.display();
}

String MenuOLED::menuToText(EMenu menu){
    switch(menu){
        case eNoMenu:
            return "NULL";
            break;
        case eMenuDrinkType:
            return "Drinks";
            break;
        case eMenuDrinks:
            return "Current Drink";
            break;
        case eMenuKeypad:
            return "Wert Eingeben";
            break;
        case eMenuMaster:
            return "Betrag Aufladen";
            break;
        case eMenuTransact:
            return "Betrag";
            break;
        default:
            return "Fault";
            break;
    }
}