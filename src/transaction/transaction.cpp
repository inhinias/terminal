#include <Arduino.h>
#include <SPI.h>
#include <Wire.h> //I2C
#include "transaction/transaction.h"
#include "helper/logging.h"
#include "crypt/crypt.h"

/***************************************************************************************************
                    Con and Destructors
****************************************************************************************************/
Transaction::Transaction(){
    
}

Transaction::~Transaction(){

}

/***************************************************************************************************
                    Public Methods
****************************************************************************************************/
rcode Transaction::init(){
    rcode c;
    c.code = eOK;

    #if CARD_MODULE == USE_MFRC522
        MFRC522 mfrc522(NFC_SS_PIN, NFC_RST_PIN);  // Create MFRC522 instance

        SPI.begin();			// Init SPI bus
        mfrc522.PCD_Init();		// Init MFRC522
        delay(4);				// Optional delay. Some boards do need more time after init to be ready, see Readme
        mfrc522.PCD_DumpVersionToSerial();	// Show details of PCD - MFRC522 Card Reader details
        log("MFRC522 Initialized", logDEBUG);
    #elif CARD_MODULE == USE_PN532
        Adafruit_PN532 nfc = Adafruit_PN532(NFC_IRQ_PIN, NFC_RST_PIN);

        nfc.begin();

        uint32_t versiondata = nfc.getFirmwareVersion();

        if (!versiondata) {
            c.code = eGENERAL_FAULT;
            c.returnMessage = "Didn't find PN532 board";
            log(c.returnMessage, logERROR);
            return c;
        }
        else{
            // Got valid data, print it!
            String pn532Version;
            pn532Version = "Found chip PN5";
            pn532Version += ((versiondata>>24) & 0xFF, HEX);
            log(pn532Version, logINFO);

            String pn532Firmware;
            pn532Firmware = "Firmware ver. ";
            pn532Firmware += ((versiondata>>16) & 0xFF); 
            pn532Firmware += ('.'); 
            pn532Firmware += ((versiondata>>8) & 0xFF);
            log(pn532Firmware, logINFO);
            
            // configure board to read RFID tags
            nfc.SAMConfig();

            log("Ready to make transcations with PN532", logINFO);
        }
    #endif
    return c;
}



rcode Transaction::makeTransaction(tTransaction& tsa){
    rcode c;

    log("Make Transaction", logDEBUG);

    rcode dbCode;
    DB database = DB();
    dbCode = database.init();
    if(dbCode.code != eOK){
        return dbCode;
    }

    database.insertTransaction(tsa);

    c.code = eOK;
    return c;
}

rcode Transaction::readCard(tCardMem& cardMem){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

bool Transaction::newCardPresent(String& uidStr){
    #if CARD_MODULE == USE_MFRC522
        //Implement other module
    #elif CARD_MODULE == USE_PN532
        Adafruit_PN532 nfc = Adafruit_PN532(NFC_IRQ_PIN, NFC_RST_PIN);

        uint8_t uid[] = {0, 0, 0, 0, 0, 0, 0};
        uint8_t uidSize;
        bool status = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidSize, 10); 

        //writeToConsole("Card Detected, Reading...");
        // Display some basic information about the card

        if(status){
            log("Found an ISO14443A card", logDEBUG);
            String UIDlength = "UID Length: ";
            UIDlength += (String)(uidSize);
            UIDlength += " bytes";
            log(UIDlength, logDEBUG);

            for (uint8_t i=0; i < uidSize; i++){
                uidStr += (String)(uid[i]);
            }
            log(uidStr, logDEBUG);
        }
    #endif

    return status;
}


/***************************************************************************************************
                    Card Setup
****************************************************************************************************/
rcode Transaction::formatCard(){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

rcode Transaction::updateCardTrailer(tTrailer trail){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}



/***************************************************************************************************
                    Transaction Handling
****************************************************************************************************/
rcode Transaction::exportToSD(tTransaction trans){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

rcode Transaction::exportToEEPROM(tTransaction trans){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

rcode Transaction::readFromSD(uint16_t transactionID, tTransaction& transaction){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}
rcode Transaction::readFromEEPROM(uint16_t transactionID, tTransaction& transaction){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}

rcode Transaction::restoreTransactionsFromSD(){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}


/***************************************************************************************************
                    Private Methods
****************************************************************************************************/
rcode Transaction::writeBlock(uint8_t blockID, String text){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}
rcode Transaction::deleteBlock(uint8_t blockID){
    rcode c;
    c.code = eGENERAL_FAULT;
    c.returnMessage = "Not Implemented";
    return c;
}


//Last Block of a sector is SpECiaL (Trailer Block), always reads 0
//uint8_t specialBlocks[50] = {0,1,2,3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63};